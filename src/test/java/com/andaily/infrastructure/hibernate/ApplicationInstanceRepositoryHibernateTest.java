package com.andaily.infrastructure.hibernate;

import com.andaily.domain.application.ApplicationInstance;
import com.andaily.domain.application.ApplicationInstanceRepository;
import com.andaily.domain.log.FrequencyMonitorLog;
import com.andaily.domain.log.MonitoringReminderLog;
import com.andaily.domain.shared.security.SecurityUtils;
import com.andaily.infrastructure.AbstractRepositoryTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * 15-1-2
 *
 * @author Shengzhao Li
 */
public class ApplicationInstanceRepositoryHibernateTest extends AbstractRepositoryTest {

    @Autowired
    private ApplicationInstanceRepository repository;


    @Test
    public void findApplicationInstanceList() {
        ApplicationInstance applicationInstance = new ApplicationInstance()
                .instanceName("Andaily");
        repository.saveOrUpdate(applicationInstance);

        Map<String, Object> map = new HashMap<>();
        map.put("user", SecurityUtils.currentUser());
        map.put("perPageSize", 20);
        map.put("startIndex", 0);
        map.put("instanceName", "A");

        List<ApplicationInstance> list = repository.findApplicationInstanceList(map);
        assertEquals(list.size(), 1);

        int i = repository.totalApplicationInstanceList(map);
        assertEquals(i, 1);
    }

    @Test
    public void findHBSearchInstances() {
        ApplicationInstance applicationInstance = new ApplicationInstance()
                .instanceName("Andaily");
        repository.saveOrUpdate(applicationInstance);

        Map<String, Object> map = new HashMap<>();
        map.put("user", SecurityUtils.currentUser());
        map.put("perPageSize", 20);
        map.put("startIndex", 0);
        map.put("key", "A");

        fullClean();

        final List<ApplicationInstance> list = repository.findHBSearchInstances(map);
        assertEquals(list.size(), 1);

        final int i = repository.totalHBSearchInstances(map);
        assertEquals(i, 1);
    }

    @Test
    public void findAllEnabledInstances() {
        ApplicationInstance applicationInstance = new ApplicationInstance()
                .instanceName("Andaily");
        repository.saveOrUpdate(applicationInstance);

        fullClean();

        List<ApplicationInstance> list = repository.findAllEnabledInstances();
        assertTrue(list.size() == 0);
    }


    @Test
    public void deleteInstanceFrequencyMonitorLogs() {
        ApplicationInstance applicationInstance = new ApplicationInstance()
                .instanceName("Andaily");
        repository.saveOrUpdate(applicationInstance);

        FrequencyMonitorLog monitorLog = new FrequencyMonitorLog()
                .instance(applicationInstance);
        repository.saveOrUpdate(monitorLog);

        fullClean();

        final int count = repository.deleteInstanceFrequencyMonitorLogs(applicationInstance.guid());
        assertEquals(count, 1);


    }

    @Test
    public void deleteInstanceMonitoringReminderLogs() {
        ApplicationInstance applicationInstance = new ApplicationInstance()
                .instanceName("Andaily");
        repository.saveOrUpdate(applicationInstance);

        FrequencyMonitorLog monitorLog = new FrequencyMonitorLog()
                .instance(applicationInstance);
        repository.saveOrUpdate(monitorLog);

        MonitoringReminderLog reminderLog = new MonitoringReminderLog()
                .instance(applicationInstance);
        repository.saveOrUpdate(reminderLog);

        fullClean();

        final int count = repository.deleteInstanceMonitoringReminderLogs(applicationInstance.guid());
        assertEquals(count, 1);


    }

    @Test
    public void testApplicationInstance() {
        ApplicationInstance applicationInstance = new ApplicationInstance()
                .instanceName("Andaily");
        repository.saveOrUpdate(applicationInstance);

        fullClean();

        ApplicationInstance instance = repository.findByGuid(applicationInstance.guid(), ApplicationInstance.class);
        assertNotNull(instance);
        assertNotNull(instance.requestMethod());
        assertNotNull(instance.instanceName());

    }

}
