package com.andaily.web.controller;

import com.andaily.domain.dto.user.UserListDto;
import com.andaily.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Shengzhao Li
 */
@Controller
@RequestMapping("/user/")
public class UserController {


    @Autowired
    private UserService userService;


    @RequestMapping("list.hb")
    public String list(UserListDto listDto, Model model) {
        userService.loadUserListDto(listDto);
        model.addAttribute("listDto", listDto);
        return "user/user_list";
    }


}