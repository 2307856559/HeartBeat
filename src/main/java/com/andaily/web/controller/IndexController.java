package com.andaily.web.controller;

import com.andaily.domain.dto.HBSearchDto;
import com.andaily.domain.dto.IndexAdditionInstanceDto;
import com.andaily.domain.dto.IndexDto;
import com.andaily.domain.dto.MonitoringInstanceDto;
import com.andaily.domain.dto.application.InstanceStatisticsDto;
import com.andaily.service.ApplicationInstanceService;
import com.andaily.service.IndexService;
import com.andaily.web.WebUtils;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

/**
 * @author Shengzhao Li
 */
@Controller
public class IndexController {


    @Autowired
    private IndexService indexService;
    @Autowired
    private ApplicationInstanceService instanceService;


    @RequestMapping("index.hb")
    public String index(IndexDto indexDto, Model model) {
        indexService.loadIndexDto(indexDto);
        model.addAttribute("indexDto", indexDto);
        return "index";
    }


    /*
    * Online load addition  monitoring data
    * */
    @RequestMapping("load_addition_monitor_logs.hb")
    public void loadAdditionData(IndexAdditionInstanceDto additionInstanceDto, HttpServletResponse response) {
        indexService.loadIndexAdditionInstanceDto(additionInstanceDto);
        WebUtils.writeJson(response, JSONObject.fromObject(additionInstanceDto));
    }


    /*
   * Monitoring instance
   * */
    @RequestMapping("monitoring/{guid}.hb")
    public String monitoringInstance(@PathVariable("guid") String guid, Model model) {
        MonitoringInstanceDto instanceDto = indexService.loadMonitoringInstanceDto(guid);
        model.addAttribute("instanceDto", instanceDto);
        return "monitoring_instance";
    }

    /*
   * Statics instance  details
   * */
    @RequestMapping("monitoring/statistics/{guid}.hb")
    public String statisticsInstance(@PathVariable("guid") String guid, Model model) {
        InstanceStatisticsDto statisticsDto = instanceService.loadInstanceStatisticsDto(guid);
        model.addAttribute("statisticsDto", statisticsDto);
        return "instance/statistics_instance";
    }

    /*
   * Search
   * */
    @RequestMapping("search.hb")
    public String search(HBSearchDto searchDto, Model model) {
        indexService.loadHBSearchDto(searchDto);
        model.addAttribute("searchDto", searchDto);
        return "search_result";
    }

}