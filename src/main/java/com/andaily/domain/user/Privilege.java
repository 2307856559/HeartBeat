package com.andaily.domain.user;

/**
 * @author Shengzhao Li
 */

public enum Privilege {

    CREATE_EDIT_INSTANCE("Create/Edit instance"),
    DELETE_INSTANCE("Delete instance"),
    START_STOP_INSTANCE("Start/Stop monitoring instance"),

    USER_MANAGEMENT("User management");


    private String label;


    private Privilege(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return name();
    }
}